const sendButton = document.querySelector('#send-button')
sendButton.addEventListener('click', sendMessage)

const ul = document.querySelector('#ul')
const textArea = document.querySelector('#user-message')

function showMessages(messageAPI) {
  const message = document.createElement('div')
  message.classList.add('message')

  message.setAttribute('id', messageAPI.id)

  message.innerHTML = `
            <p>${messageAPI.author}</p>
            <li id="li">${messageAPI.content}</li>
            <textarea rows="1" class="edit-textarea" hidden></textarea>
            <div class="buttons">
              <button class="edit-button" onclick="editarMensagem(this)" >Editar</button>
              <button class="delete-button" onclick="deletarMensagem(this)" >Excluir</button>
              <button class="confirm-button" onclick="confirmarMensagem(this)" hidden>Confirmar</button>
            </div>
  
  `

  ul.appendChild(message)
}

function sendMessage() {
  const userMessage = textArea.value

  postMessages('Gabriel Berto', userMessage)
  textArea.value = ""
}

function deletarMensagem(deleteButton) {
  const messageId = deleteButton.parentNode.parentNode.id

  deleteButton.parentNode.parentNode.remove(messageId)
  deleteMessages(messageId)
}

function editarMensagem(editButton) {


  const li = editButton.parentNode.parentNode.querySelector('#li')
  li.hidden = true

  const editMessageTextArea = editButton.parentNode.parentNode.querySelector('textarea')
  editMessageTextArea.value = li.innerText
  editMessageTextArea.hidden = false
  editMessageTextArea.focus()

  const deleteButton = editButton.parentNode.querySelector('.delete-button')
  const confirmButton = editButton.parentNode.querySelector('.confirm-button')
  deleteButton.hidden = true
  editButton.hidden = true
  confirmButton.hidden = false
}

function confirmarMensagem(confirmButton) {
  const messageId = confirmButton.parentNode.parentNode.id
  const editedMessage = confirmButton.parentNode.parentNode.querySelector('textarea').value
  const li = confirmButton.parentNode.parentNode.querySelector('#li')

  li.innerText = editedMessage

  const editMessageTextArea = confirmButton.parentNode.parentNode.querySelector('textarea')
  editMessageTextArea.hidden = true
  li.hidden = false

  const deleteButton = confirmButton.parentNode.querySelector('.delete-button')
  const editButton = confirmButton.parentNode.querySelector('.edit-button')
  deleteButton.hidden = false
  editButton.hidden = false
  confirmButton.hidden = true

  updateMessages(messageId, editedMessage)
}

// ajax e promisses
const url = 'http://treinamento-ajax-api.herokuapp.com/messages'

const getMessages = () => {
  fetch(url)
    .then(response => response.json())
    .then(messages => {
      messages.forEach(message => {
        showMessages(message)
      })
    })
    .catch(err => console.error(err))
}
getMessages()

const postMessages = (author, content) => {

  const body = {
    message: {
      author: author,
      content: content
    }
  }

  const config = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  }

  fetch(url, config)
    .then(response => response.json())
    .then(result => console.log(result))
    .catch(err => console.error(err))
}

const deleteMessages = messageId => {
  fetch(`${url}/${messageId}`, {
    method: "DELETE"
  })
    .then(response => response.json())
    .then(result => console.log(result))
    .catch(err => console.error(err))
}

const updateMessages = (messageId, content) => {
  const body = {
    message: {
      content: content
    }
  }

  const config = {
    method: "PATCH",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  }

  fetch(`${url}/${messageId}`, config)
    .then(response => response.json())
    .then(result => console.log(result))
    .catch(err => console.error(err))
}
